import './App.css';
import { useState } from 'react';
import { areOperandsEqual } from './OperandService';
import MultiplicationTable from './app/MultiplicationTable';
import MultiplicationHistory from './app/MultiplicationHistory';

const App = () => {
  const [currentProduct, setCurrentProduct] = useState({ x: undefined, y: undefined });
  const [history, setHistory] = useState([]); // array of { x: number, y: number }

  // operandObject: { x: number, y: number }
  const handleChange = (operandObject) => {
    setCurrentProduct(operandObject);

    if (history.filter(i => areOperandsEqual(i, operandObject)).length === 0) {
      const newHistory = [...history, operandObject];
      setHistory(newHistory.sort((a, b) => a.x === b.x ? a.y - b.y : a.x - b.x));
    }
  }

  return (
    <div className="App">
      <MultiplicationTable 
        handleChange={handleChange} 
        currentProduct={currentProduct} 
        xOperands={12}
        yOperands={12}
      />
      <MultiplicationHistory
        history={history}
      />
    </div>
  );
}

export default App;
