//  a: { x: number, y: number }
//  b: { x: number, y: number }
export const areOperandsEqual = (a, b) => a.x === b.x && a.y === b.y;
