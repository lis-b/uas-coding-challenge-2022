// history: array of { x: number, y: number }
const MultiplicationHistory = ({ history }) => {


    return (
        <div className="history">
            <h1>History</h1>
            {history.map((e) => (
                <div>{`${e.x} \u00D7 ${e.y} = ${e.x * e.y}`}</div>
            ))}
        </div>
    );
}

export default MultiplicationHistory;