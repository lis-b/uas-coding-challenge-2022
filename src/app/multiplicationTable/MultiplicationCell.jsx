const MultiplicationCell = ({ content, displayContent, isHeader, onClick }) => {
    return (
        <td 
            className={isHeader ? "header" : displayContent ? "" : "cursor-change"} 
            onClick={isHeader ? () => {} : onClick}
        >
            {displayContent ? content : ""}
        </td>
    );
}

export default MultiplicationCell;