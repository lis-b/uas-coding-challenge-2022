import { areOperandsEqual } from "../OperandService";
import MultiplicationCell from "./multiplicationTable/MultiplicationCell";

// handleChange: ({ x: number, y: number }) => void
//      function call to handle a click on a product table cell
// currentProduct: { x: number, y: number }
//      last product clicked in the table
// xOperands: number 
//      number of operands for rows
// yOperands: number
//      number of operands for columns
const MultiplicationTable = ({ handleChange, currentProduct, xOperands, yOperands }) => {
    let tableContent = [];

    for (let i = 0; i <= xOperands; i++) { // rows
        let tableRow = [];

        for (let j = 0; j <= yOperands; j++) { // columns
            let content;
            const operandObject = { x: i, y: j };

            if (i === 0 && j === 0) 
                content = '\u00D7';
            else if (i === 0) 
                content = j;
            else if (j === 0)
                content = i;
            else 
                content = i * j;
            
            tableRow.push(
                <MultiplicationCell 
                    key={`${i}, ${j}`}
                    content={content}
                    displayContent={i === 0 || j === 0 || areOperandsEqual(currentProduct, operandObject)}
                    isHeader={i === 0 || j === 0} 
                    onClick={() => handleChange(operandObject)}
                />
            );
        }

        tableContent.push(
            <tr className="row" key={i}>{tableRow}</tr>
        );
    }

    return <div className="multiplication-table"><table>{tableContent}</table></div>;
}

export default MultiplicationTable;