# React Advanced Multiplication Table Challenge (F-RMT-1)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). Run with `npm start`.

Or you can also view it deployed with Netlify [here](https://imaginative-horse-871954.netlify.app/).

## Notes

With my implementation, I feel this should have best been done in TypeScript, due to the object I am using to store the current product and history states so there's no confusion. I could have changed the implementation a bit for JavaScript, but I decided to keep it this way as it was more straightforward, and checks can always be added to make sure the arguments actually match what the functions are expecting.

You can change number of operands in each column and row separately.